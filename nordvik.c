/*
nordvik.c
Michael Sharp - COSC330 Assignment 2

An integer, car_balance, acts as a pendulum swinging + for East bound cars on
bridge and - for West bound cars on bridge.

The pendulum must be in a suitable position before vehicles can enter the
bridge.

Trucks travelling in any direction acquire a lock on the centred pendulum
(car_balance of 0) and keep it there while the Truck crosses.

East bound cars acquire a lock on the centred or already positively weighted
pendulum,incrementing it and releasing it. Once accross the bridge the pendulum
is safely decremented.

West bound cars acquire a lock on the centred or already negatively weighted
pendulum, decrementing it and releasing it. Once accross the bridge the pendulum
is safely incremented.

Change VEHICLES for different numbers of vehicles on the road.

The types and directions of vehicles is randomly generated.
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "semaphore.h"

// number of vehicles to place on the road
#define VEHICLES 14

// wait time constants
#define MAXWAIT 20
#define CROSSINGTIME 2

// vehicle type identifiers
#define CAR 0
#define TRUCK 1

// direction identifiers
#define EAST 0
#define WEST 1

static semaphore_p sem;

// access protected by sem
// acts as a pendulum
// swings + for East bound cars on bridge
// swings - for West bound cars on bridge
int car_balance = 0;

/*
 * vehicle struct passed to thread entry point
 */
typedef struct vehicle {
  int id;
  int direction;
  int type;
} vehicle;

/*
 * string representation of vehicle type
 */
char *v_type_desc(int *i) {
  if (*i == 0)
    return "Car";

  return "Truck";
}

/*
 * string representation of vehicle direction
 */
char *v_dir_desc(int *i) {
  if (*i == 0)
    return "East";

  return "West";
}

/*
 * thread entry and initialiser of vehice travel
 */
void *travel(void *arg) {
  // vehicle arg
  vehicle v = *((vehicle *)arg);

  // random sleep to mix up vehicle order
  sleep(rand() % MAXWAIT);

  // handle different vehicle types
  if (v.type == CAR) {

    // check car_balance until conditions are right for this Car to cross the
    // bridge
    for (;;) {
      waitSemaphore(sem);
      if ((v.direction == EAST && car_balance < 0) ||
          (v.direction == WEST && car_balance > 0)) {
        postSemaphore(sem);
        continue;
      }
      break;
    }

    // swing the pendulum in the appropriate direction and release the lock
    v.direction == EAST ? car_balance++ : car_balance--;
    fprintf(stderr, "Car (id %d) going %s on bridge\n", v.id,
            v_dir_desc(&v.direction));
    postSemaphore(sem);

    // cross the bridge
    sleep(CROSSINGTIME);

    // get the lock and swing the pendulum back
    waitSemaphore(sem);
    v.direction == EAST ? car_balance-- : car_balance++;
    fprintf(stderr, "Car (id %d) going %s off bridge\n", v.id,
            v_dir_desc(&v.direction));

    // remove the lock
    postSemaphore(sem);
  } else if (v.type == TRUCK) {

    // check car_balance until conditions are right for this Truck to cross the
    // bridge
    for (;;) {
      waitSemaphore(sem);
      if (car_balance == 0)
        break;
      postSemaphore(sem);
    }

    // while maintaining the lock, cross the bridge then release the lock
    fprintf(stderr, "Truck (id %d) going %s on bridge\n", v.id,
            v_dir_desc(&v.direction));
    // cross the bridge
    sleep(CROSSINGTIME);
    fprintf(stderr, "Truck (id %d) going %s off bridge\n", v.id,
            v_dir_desc(&v.direction));
    postSemaphore(sem);
  }

  pthread_exit(NULL);
}

/*
 * main
 */
int main(int argc, char *argv[]) {
  pthread_t tid[VEHICLES];
  vehicle *vehicles = (vehicle *)calloc(VEHICLES, sizeof(vehicle));

  // seed random number generator
  srand(time(NULL));

  int i;

  // create vehices of random types and directions
  for (i = 0; i < VEHICLES; i++) {
    vehicles[i].id = i;
    vehicles[i].direction = (rand() % 2);
    vehicles[i].type = (rand() % 2);

    fprintf(stderr, "Vehicle id %d is a %s travelling %s\n", vehicles[i].id,
            v_type_desc(&vehicles[i].type), v_dir_desc(&vehicles[i].direction));
  }

  fprintf(stderr, "\n");

  // initialise semaphore
  sem = newSemaphore(1);

  // create a thread for each vehicle
  for (i = 0; i < VEHICLES; i++)
    if (pthread_create(&tid[i], NULL, travel, (void *)&vehicles[i])) {
      perror("Could not create thread for vehicle");
      exit(EXIT_FAILURE);
    }

  // wait for all threads to do their thing
  for (i = 0; i < VEHICLES; i++)
    pthread_join(tid[i], NULL);

  // tear down semaphore
  freeSemaphore(sem);

  return 0;
}
