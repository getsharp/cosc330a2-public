COMPILER = gcc
CFLAGS = -Wall -pedantic -pthread
COBJS = semaphore.o
CEXES = nordvik

all: ${CEXES}

nordvik: nordvik.c ${COBJS}
	${COMPILER} ${CFLAGS} nordvik.c ${COBJS} -o nordvik

%.o: %.c %.h  makefile
	${COMPILER} ${CFLAGS} $< -c

clean:
	rm -f *.o *~ ${CEXES}
