#include <stdlib.h>
#include <stdio.h>
#include "semaphore.h"

// create a semaphore
semaphore_p newSemaphore(int value) {
  semaphore_p retval = (semaphore_p)calloc(1, sizeof(semaphore_t));
  if (retval == NULL)
    return NULL;

  if (pthread_mutex_init(&retval->mutex, NULL)) {
    perror("pthread_mutex_init failed");
    exit(EXIT_FAILURE);
  }

  if (pthread_cond_init(&retval->cond, NULL)) {
    perror("pthread_cond_init failed");
    exit(EXIT_FAILURE);
  }

  retval->value = (value >= 0) ? value : 0;
  return retval;
}

// destroy semaphore
void freeSemaphore(semaphore_p sem) {
  if (pthread_cond_destroy(&sem->cond)) {
    perror("pthread_cond_destroy failed");
    exit(EXIT_FAILURE);
  }

  if (pthread_mutex_destroy(&sem->mutex)) {
    perror("pthread_mutex_destroy failed");
    exit(EXIT_FAILURE);
  }

  free(sem);
}

int postSemaphore(semaphore_p sem) {
  if (pthread_mutex_lock(&sem->mutex)) {
    perror("pthread_mutex_lock failed");
    exit(EXIT_FAILURE);
  }

  sem->value++;

  if (pthread_cond_signal(&sem->cond)) {
    perror("pthread_cond_signal failed");
    exit(EXIT_FAILURE);
  }

  if (pthread_mutex_unlock(&sem->mutex)) {
    perror("pthread_mutex_unlock failed");
    exit(EXIT_FAILURE);
  }
  return 0;
}

int waitSemaphore(semaphore_p sem) {
  if (pthread_mutex_lock(&sem->mutex)) {
    perror("pthread_mutex_lock failed");
    exit(EXIT_FAILURE);
  }
  while (sem->value == 0)
    pthread_cond_wait(&sem->cond, &sem->mutex);
  sem->value--;

  if (pthread_mutex_unlock(&sem->mutex)) {
    perror("pthread_mutex_unlock failed");
    exit(EXIT_FAILURE);
  }

  return 0;
}
