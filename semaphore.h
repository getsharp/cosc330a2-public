#include <pthread.h> /* for pthread_mutex_t etc */

typedef struct semaphore {
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  int value;
} semaphore_t;

typedef semaphore_t *semaphore_p;

semaphore_p newSemaphore(int value);
int postSemaphore(semaphore_p);
int waitSemaphore(semaphore_p);
void freeSemaphore(semaphore_p);
